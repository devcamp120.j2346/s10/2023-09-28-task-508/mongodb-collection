const express = require("express");

const router = express.Router();

const {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getCourseByIDMiddleware,
    deleteCourseMiddleware,
    updateCourseMiddleware
} = require("../middlewares/course.middleware");

router.get("/", getAllCourseMiddleware, (req, res) => {
    res.json({
        message: "GET all courses"
    })
});

router.post("/", createCourseMiddleware, (req, res) => {
    res.json({
        message: "POST course"
    })
})

router.get("/:courseid", getCourseByIDMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.json({
        message: "GET course id = " + courseid
    })
})

router.put("/:courseid", updateCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.json({
        message: "PUT course id = " + courseid
    })
})

router.delete("/:courseid", deleteCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.json({
        message: "DELETE course id = " + courseid
    })
})

module.exports = router;